cmake_minimum_required(VERSION 3.10)
project(numanal2 VERSION 1.0)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_compile_options(
    -pedantic
    -Wall
    -Wextra
    -Wcast-align
    -Wcast-qual
    -Wnon-virtual-dtor
    -Wdisabled-optimization
    -Wformat=2
    -Winit-self
    -Wmissing-declarations
    -Wmissing-include-dirs
    -Wold-style-cast
    -Woverloaded-virtual
    -Wredundant-decls
    -Wshadow
    -Wsign-conversion
    -Wconversion
    -Wdouble-promotion
    -Wnull-dereference
    -Wsign-promo
    -Wstrict-overflow=5
    -Wunused
    -Wswitch-default
    -Wctor-dtor-privacy
    -Wundef
)

FILE(GLOB SRC_FILES *.cpp)
add_executable(numanal2 ${SRC_FILES})
target_include_directories(numanal2 PRIVATE include)
