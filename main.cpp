#include <chrono>
#include <iomanip>
#include <iostream>

#include "Quadrature.h"

inline double f(double x) {
    return 1 / (0.01 + pow(x - 0.3, 2)) + 1 / (0.04 + pow(x - 0.9, 2)) - 6;
}

inline double groundTruth = 29.858325395498671;

int main() {
    std::cout << "Exercise1: Numerical integrations.\n";
    std::cout << "Analytical result for reference:\t\t" << std::setprecision(15) << groundTruth << "\n";
    std::cout << "\n";

    std::cout << "Simpson's Quadrature:\t\t\t\t" << std::setprecision(15) << SimpsonsQuadrature(f, 0, 1, 1000) << "\n";
    std::cout << "\n";

    std::cout << "Equidistant quadrature solutions:\n";
    std::cout << "M=2, p=2, w0=w1=0.5:\t\t\t\t" << std::setprecision(15) << equidistantQuadrature(f, 0, 1, 1000, M2())
              << "\n";
    std::cout << "M=3, p=4, w0=w2=0.17, w1=0.64:\t\t\t" << std::setprecision(15)
              << equidistantQuadrature(f, 0, 1, 1000, M3()) << "\n";
    std::cout << "M=4, p=4, w0=w3=0.125, w1=w2=0.325:\t\t" << std::setprecision(15)
              << equidistantQuadrature(f, 0, 1, 1000, M4()) << "\n";
    std::cout << "M=5, p=6, w0=w4=0.078, w1=w3=0.355, w2=0.134:\t" << std::setprecision(15)
              << equidistantQuadrature(f, 0, 1, 1000, M5()) << "\n";
    std::cout << "\n";

    std::cout << "Adaptive quadrature solutions:\n";
    std::cout << "M=2, p=2, w0=w1=0.5:\t\t\t\t" << std::setprecision(15)
              << std::get<0>(adaptiveQuadrature(f, 0, 1, 1e-8, M2())) << "\n";
    std::cout << "M=3, p=4, w0=w2=0.17, w1=0.64:\t\t\t" << std::setprecision(15)
              << std::get<0>(adaptiveQuadrature(f, 0, 1, 1e-8, M3())) << "\n";
    std::cout << "M=4, p=4, w0=w3=0.125, w1=w2=0.325:\t\t" << std::setprecision(15)
              << std::get<0>(adaptiveQuadrature(f, 0, 1, 1e-8, M4())) << "\n";
    std::cout << "M=5, p=6, w0=w4=0.078, w1=w3=0.355, w2=0.134:\t" << std::setprecision(15)
              << std::get<0>(adaptiveQuadrature(f, 0, 1, 1e-8, M5())) << "\n";
    std::cout << "\n";

    std::cout
        << "Excercise2: Comparing the performance of equidistant and adaptive quadratures with M2 quadrature rule.\n";
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    auto [adaptive_result, intervals] = adaptiveQuadrature(f, 0, 1, 1e-8, M2());
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "AdaptiveQuadrature:\tResult\t" << adaptive_result << "\tRuntime\t"
              << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]\n";

    begin = std::chrono::steady_clock::now();
    double equidist_result = equidistantQuadrature(f, 0, 1, size_t(intervals), M2());
    end = std::chrono::steady_clock::now();
    std::cout << "EquidistantQuadrature:\tResult\t" << equidist_result << "\tRuntime\t"
              << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]\n";
    std::cout << "\n";

    std::cout << "Excercise3: 2-point Gaussian quadrature.\n";
    std::cout << "Using Chebishev polynomial:\t\t\t" << twoPointGaussQuadrature(f, 0, 1, 1000) << "\n";
    std::cout << "\n";

    std::cout << "Excercise4: Error comparison.\n";
    std::cout << "M2:\t\t" << std::setprecision(4) << abs(equidistantQuadrature(f, 0, 1, 1000, M2()) - groundTruth)
              << "\n";
    std::cout << "M3:\t\t" << std::setprecision(4) << abs(equidistantQuadrature(f, 0, 1, 1000, M3()) - groundTruth)
              << "\n";
    std::cout << "M4:\t\t" << std::setprecision(4) << abs(equidistantQuadrature(f, 0, 1, 1000, M4()) - groundTruth)
              << "\n";
    std::cout << "M5:\t\t" << std::setprecision(4) << abs(equidistantQuadrature(f, 0, 1, 1000, M5()) - groundTruth)
              << "\n";
    std::cout << "Gaussian:\t" << std::setprecision(4) << abs(twoPointGaussQuadrature(f, 0, 1, 1000) - groundTruth)
              << "\n";
    std::cout
        << "M2 and M3 have similar error rates to the Gaussian. Interestingly, M4 has the smallest error rate, not M5.\n";

    return 0;
}
