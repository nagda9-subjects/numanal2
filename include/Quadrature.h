#pragma once
#include <assert.h>
#include <tuple>

#include <functional>
#include <math.h>

typedef std::function<double(double)> RealFunc;
typedef std::function<double(RealFunc f, double a, double b, size_t N)> Quadrature;

inline double transformQuadraturePoint(double a_from, double b_from, double a_to, double b_to, double x_from) {
    return (b_from - x_from) * a_to / (b_from - a_from) + (x_from - a_from) * b_to / (b_from - a_from);
}

inline double transformQuadratureWeight(double a_from, double b_from, double a_to, double b_to, double w_from) {
    return (b_to - a_to) * w_from / (b_from - a_from);
}

class QuadratureRule {
 public:
    std::vector<double> weights{};
    size_t p{};
};

class M2 : public QuadratureRule {
 public:
    M2() {
        weights = {0.5, 0.5};
        p = 2;
    }
};

class M3 : public QuadratureRule {
 public:
    M3() {
        weights = {0.17, 0.66, 0.17};
        p = 4;
    }
};

class M4 : public QuadratureRule {
 public:
    M4() {
        weights = {0.125, 0.375, 0.375, 0.125};
        p = 4;
    }
};

class M5 : public QuadratureRule {
 public:
    M5() {
        weights = {0.078, 0.355, 0.134, 0.355, 0.078};
        p = 6;
    }
};

inline double equidistantQuadrature(RealFunc f, double a, double b, size_t N, QuadratureRule rule) {
    double h{1 / double(N)};
    double sum{0};
    size_t M = rule.weights.size();

    for (size_t i = 0; i <= N * (M - 1); i++) {
        double x{(double(i) * h / double(M - 1))};
        double w{(i == 0 || i == N * (M - 1)) ? rule.weights[0] * h
                 : (i % (M - 1) == 0)         ? 2 * rule.weights[0] * h
                                              : rule.weights[i % (M - 1)] * h};

        double x_hat = transformQuadraturePoint(0, 1, a, b, x);
        double w_hat = transformQuadratureWeight(0, 1, a, b, w);

        sum += f(x_hat) * w_hat;
    }
    return sum;
}

inline std::tuple<double, int> adaptiveQuadrature(RealFunc f, double a, double b, double eps, QuadratureRule rule) {
    assert(eps > 0);
    assert(a <= b);

    double h{1};
    size_t M = rule.weights.size();

    double Q{};
    double Q1{};
    double Q2{};
    size_t intervals{};

    for (size_t i = 0; i <= M - 1; i++) {
        double x{double(i) * h / double(M - 1)};
        double w{(i == 0 || i == M - 1) ? rule.weights[0] * h
                 : (i % (M - 1) == 0)   ? 2 * rule.weights[0] * h
                                        : rule.weights[i % (M - 1)] * h};
        double x_hat = transformQuadraturePoint(0, 1, a, b, x);
        double w_hat = transformQuadratureWeight(0, 1, a, b, w);
        Q += f(x_hat) * w_hat;

        x_hat = transformQuadraturePoint(0, 1, a, (a + b) / 2, x);
        w_hat = transformQuadratureWeight(0, 1, a, (a + b) / 2, w);
        Q1 += f(x_hat) * w_hat;

        x_hat = transformQuadraturePoint(0, 1, (a + b) / 2, b, x);
        w_hat = transformQuadratureWeight(0, 1, (a + b) / 2, b, w);
        Q2 += f(x_hat) * w_hat;
    }

    double heur = abs(Q1 + Q2 - Q) / (pow(2, rule.p) - 1);

    if (heur >= eps) {
        size_t interval1{};
        size_t interval2{};
        std::tie(Q1, interval1) = adaptiveQuadrature(f, a, (a + b) / 2, eps / 2, rule);
        std::tie(Q2, interval2) = adaptiveQuadrature(f, (a + b) / 2, b, eps / 2, rule);
        intervals = interval1 + interval2;
    } else {
        intervals = 1;
    }

    return {Q1 + Q2, intervals};
}

inline double twoPointGaussQuadrature(RealFunc f, double a, double b, size_t N) {
    /**
    * The quadrature rule is an n-point Gaussian quadrature rule, when x1,x2,...,xn are the
    * zeros of an nth orthogonal polynomial on [a, b], with weights calculated from the 
    * Vandermonde system: ∑_{j=1}^{n}(ω_j*x_j^m) = ∫_{a}^{b}(x^m)dx, m = 1,...,n-1
    *
    * Lets use the nth Chebyshev polynomial as the orthogonal polynomial, denoted as T[n](x), 
    * defined on the interval [a, b] = [-1, 1]. It has n zeros within the interval.
    *
    * The Chebyshev polynomials satisfy the following recurrence relation:
    * T[0](x) = 1
    * T[1](x) = x
    * T[n+1](x) = 2xT[n](x) - T[n-1](x)
    *
    * For n = 2: 
    * T[2](x) = 2xT[1](x) - T[0](x) = 2x^2 - 1
    *  
    * To find the zeros of this polynomial, set T[2](x) = 0:
    * 2x^2 - 1 = 0
    * x^2 = 1/2
    *
    * So the zeros of the polynomial, as well as the two quadrature points are:
    * x1, x2 = 󰦒 √2/2
    *
    * To get the corresponding weights, we have to solve the Vandermonde system:
    * w1 * x1^0 + w2 * x2^0 = ∫_{-1}^{1}(x^0) = 2
    * w1 * x2^1 + w2 * x2^1 = ∫_{-1}^{1}(x^1) = 0
    *
    * This means:
    * w1 * 1 + w2 * 1 = 2
    * -w1 * √2/2 + w2 * √2/2 = 0
    *
    * Therefore w1 = w2 = 1
    **/

    double h{(b - a) / double(N)};  // [-1, 1] interval divided to N pieces
    double x1{-sqrt(2) / 2};
    double x2{sqrt(2) / 2};
    double w{1};
    double sum{0};

    for (size_t i = 0; i < N; i++) {
        double x1_hat = transformQuadraturePoint(-1, 1, a + double(i) * h, a + double(i + 1) * h, x1);
        double x2_hat = transformQuadraturePoint(-1, 1, a + double(i) * h, a + double(i + 1) * h, x2);
        double w_hat = transformQuadratureWeight(-1, 1, a + double(i) * h, a + double(i + 1) * h, w);
        sum += f(x1_hat) * w_hat + f(x2_hat) * w_hat;
    }

    return sum;
}

inline double midpointQuadrature(RealFunc f, double a, double b, size_t N) {
    double h{1 / double(N)};
    double sum{0};
    for (size_t i = 0; i <= N - 1; i++) {
        double x{(double(i) + 0.5) * h};
        double w{h};

        double x_hat = transformQuadraturePoint(0, 1, a, b, x);
        double w_hat = transformQuadratureWeight(0, 1, a, b, w);

        sum += f(x_hat) * w_hat;
    }
    return sum;
}

inline double trapezoidalQuadrature(RealFunc f, double a, double b, size_t N) {
    double h{1 / double(N)};
    double sum{0};
    for (size_t i = 0; i <= N; i++) {
        double x{double(i) * h};
        double w{(i == 0 || i == N) ? h / 2 : h};

        double x_hat = transformQuadraturePoint(0, 1, a, b, x);
        double w_hat = transformQuadratureWeight(0, 1, a, b, w);

        sum += f(x_hat) * w_hat;
    }
    return sum;
}

inline double SimpsonsQuadrature(RealFunc f, double a, double b, size_t N) {
    double h{1 / double(N)};
    double sum{0};
    for (size_t i = 0; i <= 2 * N; i++) {
        double x{double(i) * h / 2};
        double w{(i == 0 || i == 2 * N) ? h / 6 : (i % 2 == 0) ? 2 * h / 6 : 4 * h / 6};

        double x_hat = transformQuadraturePoint(0, 1, a, b, x);
        double w_hat = transformQuadratureWeight(0, 1, a, b, w);

        sum += f(x_hat) * w_hat;
    }
    return sum;
}

inline bool calcConvergenceRate(Quadrature q,
                                RealFunc f,
                                double from,
                                double to,
                                double groundTruth,
                                double precision,
                                size_t maxIter,
                                double* convergenceRate) {
    size_t i = 1;
    double err1 = groundTruth - q(f, from, to, i);
    double err2 = groundTruth - q(f, from, to, 2 * i);
    double r1 = log2(err1 / err2);
    while (true) {
        i++;
        err1 = groundTruth - q(f, from, to, i);
        err2 = groundTruth - q(f, from, to, 2 * i);
        double r2 = log2(err1 / err2);

        if (abs(r1 - r2) < precision) {
            *convergenceRate = r2;
            return true;
        }

        if (i > maxIter) {
            return false;
        }

        r1 = r2;
    }
}
